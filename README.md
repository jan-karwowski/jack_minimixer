# jack_minimixer

A simple mixer for [Jack](https://jackaudio.org/) that can have any number of inputs, each either mono or stereo and any number of outputs (also mono or stereo). The mixer has wxWidgets interface with sliders to adjust volume of channels. Channels are configured by a config file.

## Missing features

 - balance of stereo channels
 - clipping indication
 - gui for adding/removing channels
 - saving the state of the mixer
 - setting mixer sliders range
 
## Config file

The config file is currently the only way of configuring the mixer. The default location is in `xdg_config_home`, relative path `jack_minimixer/ports.json` (usually `$HOME/.config/jack_minimixer/ports.json`).

The syntax is a json array of objects, each object has 3 fields: 
 - `type` --- one of `"input_stereo"`,`"input_mono"``"output_stereo"``"output_mono"` 
 - `name` --- a string that defines name of the porst
 - `initial_db_gain` --- a number defining the initial gain set on the mixer for the channel

Example: 
```json
[
    {
	"type": "input_stereo",
	"name": "Main input",
	"initial_db_gain": 0
    },
    {
	"type": "input_mono",
	"name": "Aux input",
	"initial_db_gain": -10
    },
    {
	"type": "output_stereo",
	"name": "Headphones ouput",
	"initial_db_gain": -27
    },
    {
	"type": "output_stereo",
	"name": "Room output",
	"initial_db_gain": 0
    }
]

```

## Program options

Besides the standard wxWidgets options the program accepts the following

- `-c PATH`, `--config PATH` a path to a config file, optional
