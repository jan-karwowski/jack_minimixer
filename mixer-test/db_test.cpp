#define BOOST_TEST_MODULE lib_test

#include <boost/test/data/test_case.hpp>
#include <boost/test/unit_test.hpp>
#include <limits>
#include <rapidcheck/boost_test.h>
#include <utility>
#include <vector>

#include "db_calc.hpp"

using namespace jack_minimixer;

BOOST_AUTO_TEST_SUITE(DecibelTest)

RC_BOOST_PROP(decibel_to_ratio_to_decibel_almost_identity,
              (unsigned long decibel)) {
  // RC does not suppot yet floating point range limited generators
  typedef std::numeric_limits<decltype(decibel)> ll;
  float db = ((float)(decibel - (ll::max() / 2)) / (float)ll::max()) * 100;
  float double_conv = multiplier_to_db_gain(db_gain_to_multiplier(db));
  RC_ASSERT(std::abs(double_conv - db) < 1e-3);
}

RC_BOOST_PROP(ratio_to_decibel_to_ratio_almost_identity,
              (unsigned long multiplier)) {
  typedef std::numeric_limits<decltype(multiplier)> ll;
  float ml = ((float)(multiplier) / (float)ll::max()) * 5;
  float double_conv = db_gain_to_multiplier(multiplier_to_db_gain(ml));
  RC_ASSERT(std::abs(double_conv - ml) < 1e-3);
}

std::vector<float> db_gains = {-20, -18, -15, -12,  -10,   -9,   -6,
                               -3,  -2,  -1,  -0.5, -0.25, -0.1, 0};
std::vector<float> multipliers = {0.1,  0.126, 0.178, 0.251, 0.316, .355, .501,
                                  .708, .794,  .891,  .944,  .971,  .989, 1.0};

BOOST_DATA_TEST_CASE(decibel_to_ratio_table, db_gains ^ multipliers, db_gain,
                     multiplier) {

  BOOST_TEST(std::abs(db_gain_to_multiplier(db_gain) - multiplier) < 1e-3);
}

BOOST_DATA_TEST_CASE(ratio_to_decibel_table, db_gains ^ multipliers, db_gain,
                     multiplier) {
  BOOST_TEST(std::abs(multiplier_to_db_gain(multiplier) - db_gain) < 1e-2);
}

BOOST_AUTO_TEST_SUITE_END()
