#ifndef DB_CALC_H
#define DB_CALC_H

#include <cmath>

namespace jack_minimixer {

inline static float db_gain_to_multiplier(float db_gain) {
  return powf(10.0f, 0.05f * db_gain);
}

inline static float multiplier_to_db_gain(float multiplier) {
  return 20.0f * log10f(multiplier);
}
} // namespace jack_minimixer

#endif /* DB_CALC_H */
