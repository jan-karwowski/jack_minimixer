#include <algorithm>
#include <chrono>
#include <concepts>
#include <cstring>
#include <exception>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <mutex>
#include <sstream>
#include <thread>
#include <type_traits>
#include <variant>
#include <vector>

#include <wx/app.h>
#include <wx/cmdline.h>
#include <wx/frame.h>
#include <wx/gbsizer.h>
#include <wx/panel.h>
#include <wx/slider.h>
#include <wx/stattext.h>
#include <wx/stdpaths.h>
#include <wx/window.h>

#include <jack/jack.h>

#include "db_calc.hpp"

#include <nlohmann/json.hpp>

template <class T> constexpr bool always_false = false;

namespace jack_minimixer {

// TODO more specific exceptions
class jack_exception : public std::exception {
  std::string function_name;

public:
  template <class StringLike>
  jack_exception(StringLike &&function_name)
      : function_name(std::forward<StringLike>(function_name)) {}

  const char *what() const noexcept override { return function_name.c_str(); }
};

struct jack_port_base {
  double amplification;
  const std::string name;

  jack_port_base(const std::string &name) : amplification(1), name(name) {}
};

struct jack_stereo_port : public jack_port_base {
  jack_port_t *left;
  jack_port_t *right;

  jack_stereo_port(const std::string &name) : jack_port_base(name) {}
};

struct jack_mono_port : public jack_port_base {
  jack_port_t *port;

  jack_mono_port(const std::string &name) : jack_port_base(name) {}
};

typedef std::variant<jack_mono_port, jack_stereo_port> jack_mixer_port;

struct input_port_tag {
  inline static const JackPortFlags flags = JackPortIsInput;
};

struct output_port_tag {
  inline static const JackPortFlags flags = JackPortIsOutput;
};

struct stereo_tag {
  inline static jack_stereo_port create_port(jack_client_t *client,
                                             const std::string &port_name,
                                             JackPortFlags flags,
                                             float multiplier = 1) {
    jack_stereo_port ret(port_name);
    ret.amplification = multiplier;
    ret.left = jack_port_register(client, (port_name + "L").c_str(),
                                  JACK_DEFAULT_AUDIO_TYPE, flags, 0);
    if (ret.left == nullptr) {
      throw new jack_exception("jack_port_register");
    }
    ret.right = jack_port_register(client, (port_name + "R").c_str(),
                                   JACK_DEFAULT_AUDIO_TYPE, flags, 0);
    if (ret.right == nullptr) {
      throw new jack_exception("jack_port_register");
    }
    return ret;
  }
};
struct mono_tag {
  inline static jack_mono_port create_port(jack_client_t *client,
                                           const std::string &port_name,
                                           JackPortFlags flags,
                                           float multiplier = 1) {
    jack_mono_port ret(port_name);
    ret.amplification = multiplier;
    ret.port = jack_port_register(client, port_name.c_str(),
                                  JACK_DEFAULT_AUDIO_TYPE, flags, 0);
    if (ret.port == nullptr) {
      throw new jack_exception("jack_port_register");
    }
    return ret;
  }
};

template <class T>
concept channels_tag =
    (std::is_same_v<T, stereo_tag> || std::is_same_v<T, mono_tag>)&&requires(
        jack_client_t *client, const std::string &port_name,
        JackPortFlags flags, float multiplier) {
      {
        T::create_port(client, port_name, flags)
      } -> std::derived_from<jack_port_base>;
      {
        T::create_port(client, port_name, flags, multiplier)
      } -> std::derived_from<jack_port_base>;
    };

template <class T>
concept direction_tag =
    (std::is_same_v<T, input_port_tag> || std::is_same_v<T, output_port_tag>)&&
  requires()
{
  { T::flags } -> std::same_as<const JackPortFlags &>;
};

static void add_to_channel(std::vector<jack_default_audio_sample_t> &channel,
                           jack_default_audio_sample_t *portdata, float gain) {
  std::transform(
      channel.begin(), channel.end(), portdata, channel.begin(),
      [gain](jack_default_audio_sample_t a, jack_default_audio_sample_t b) {
        return a + b * gain;
      });
}

struct port_vector_base {
  std::vector<jack_mixer_port> ports;
  std::mutex mutex;
};

template <direction_tag T> struct port_vector : public port_vector_base {};

class jack_mixer : public port_vector<input_port_tag>,
                   port_vector<output_port_tag> {
  jack_client_t *client;

  void run_mixing(jack_nframes_t frames) {
    std::vector<jack_default_audio_sample_t> left(frames, 0.0);
    std::vector<jack_default_audio_sample_t> right(frames, 0.0);

    // Input -> left, right
    {
      std::unique_lock<std::mutex> lock(get_mutex<input_port_tag>());
      for (const auto &vrt : get_ports<input_port_tag>()) {
        std::visit(
            [&left, &right, frames](const auto &port) {
              typedef std::decay_t<decltype(port)> port_type;
              if constexpr (std::is_same_v<port_type, jack_mono_port>) {
                jack_default_audio_sample_t *port_data =
                    static_cast<jack_default_audio_sample_t *>(
                        jack_port_get_buffer(port.port, frames));
                add_to_channel(left, port_data, port.amplification);
                add_to_channel(right, port_data, port.amplification);
              } else if constexpr ((std::is_same_v<port_type,
                                                   jack_stereo_port>)) {
                jack_default_audio_sample_t *left_port_data =
                    static_cast<jack_default_audio_sample_t *>(
                        jack_port_get_buffer(port.left, frames));
                jack_default_audio_sample_t *right_port_data =
                    static_cast<jack_default_audio_sample_t *>(
                        jack_port_get_buffer(port.right, frames));
                add_to_channel(left, left_port_data, port.amplification);
                add_to_channel(right, right_port_data, port.amplification);
              } else {
                static_assert(always_false<port_type>, "Unsupported port type");
              }
            },
            vrt);
      }
    }

    // left, right ->  Output
    {
      std::unique_lock<std::mutex> lock(get_mutex<output_port_tag>());
      for (const auto &vrt : get_ports<output_port_tag>()) {
        std::visit(
            [&left, &right, frames](const auto &port) {
              typedef std::decay_t<decltype(port)> port_type;
              if constexpr (std::is_same_v<port_type, jack_mono_port>) {
                jack_default_audio_sample_t *port_data =
                    static_cast<jack_default_audio_sample_t *>(
                        jack_port_get_buffer(port.port, frames));
                std::transform(left.begin(), left.end(), right.begin(),
                               port_data, [&port](auto a, auto b) {
                                 return (a + b) * port.amplification / 2;
                               });
              } else if constexpr ((std::is_same_v<port_type,
                                                   jack_stereo_port>)) {
                jack_default_audio_sample_t *left_port_data =
                    static_cast<jack_default_audio_sample_t *>(
                        jack_port_get_buffer(port.left, frames));
                jack_default_audio_sample_t *right_port_data =
                    static_cast<jack_default_audio_sample_t *>(
                        jack_port_get_buffer(port.right, frames));
                std::transform(
                    left.begin(), left.end(), left_port_data,
                    [&port](auto a) { return a * port.amplification; });
                std::transform(
                    right.begin(), right.end(), right_port_data,
                    [&port](auto a) { return a * port.amplification; });
              } else {
                static_assert(always_false<port_type>, "Unsupported port type");
              }
            },
            vrt);
      }
    }
  }

  static int process_callback(jack_nframes_t frames, void *arg) {
    static_cast<jack_mixer *>(arg)->run_mixing(frames);
    return 0;
  }

  static void shutdown_info_callback(jack_status_t code, const char *reason,
                                     void *arg) {
    auto &fun = static_cast<jack_mixer *>(arg)->shutdown_callback;
    if (fun) {
      fun.value()(code, reason);
    }
  }

public:
  std::optional<std::function<void(jack_status_t code, const char *reason)>>
      shutdown_callback;

  jack_mixer() {
    client = jack_client_open("jack_minimixer", JackNoStartServer, nullptr);
    if (client == nullptr) {
      throw new jack_exception("jack_client_open");
    }
    jack_set_process_callback(client, process_callback, this);
    jack_on_info_shutdown(client, shutdown_info_callback, this);
  }

  ~jack_mixer() { jack_client_close(client); }

  template <direction_tag T> port_vector_base &get_port_vector() {
    port_vector<T> *pt = this;
    return *pt;
  }

  template <direction_tag T> std::vector<jack_mixer_port> &get_ports() {
    port_vector<T> *pt = this;
    return pt->ports;
  }

  template <direction_tag T> std::mutex &get_mutex() {
    port_vector<T> *pt = this;
    return pt->mutex;
  }

  template <channels_tag C, direction_tag D>
  void add_port(const std::string &port_name, float multiplier = 1) {
    auto port = C::create_port(client, port_name, D::flags, multiplier);
    get_ports<D>().push_back(std::move(port));
  }

  void activate() { jack_activate(client); }
};

} // namespace jack_minimixer

namespace jack_minimixer::config {

struct parse_config_exception : public std::exception {
  std::string message;

  parse_config_exception(const std::filesystem::path &path) {
    std::stringstream ss;
    ss << "Error opening config file " << path;
    message = ss.str();
  }

  const char *what() const noexcept override { return message.c_str(); }
};

enum port_type { INPUT_MONO, INPUT_STEREO, OUTPUT_MONO, OUTPUT_STEREO };
struct mixer_port_config {
  port_type type;
  std::string name;
  float initial_db_gain;
};

NLOHMANN_JSON_SERIALIZE_ENUM(port_type, {
                                            {INPUT_MONO, "input_mono"},
                                            {INPUT_STEREO, "input_stereo"},
                                            {OUTPUT_MONO, "output_mono"},
                                            {OUTPUT_STEREO, "output_stereo"},
                                        })

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(mixer_port_config, type, name,
                                   initial_db_gain)

void add_ports_from_config(jack_mixer &mixer, const mixer_port_config &config) {
  float multiplier = db_gain_to_multiplier(config.initial_db_gain);
  switch (config.type) {
  case INPUT_MONO:
    mixer.add_port<mono_tag, input_port_tag>(config.name, multiplier);
    break;
  case INPUT_STEREO:
    mixer.add_port<stereo_tag, input_port_tag>(config.name, multiplier);
    break;
  case OUTPUT_MONO:
    mixer.add_port<mono_tag, output_port_tag>(config.name, multiplier);
    break;
  case OUTPUT_STEREO:
    mixer.add_port<stereo_tag, output_port_tag>(config.name, multiplier);
    break;
  }
}

void add_ports_from_config(jack_mixer &mixer,
                           const std::vector<mixer_port_config> &config) {
  for (const auto &c : config) {
    add_ports_from_config(mixer, c);
  }
}

void add_ports_from_config(jack_mixer &mixer,
                           const std::filesystem::path &config_path) {
  nlohmann::json j;
  std::ifstream stream(config_path);
  if (!stream) {
    throw new parse_config_exception(config_path);
  }

  stream >> j;
  std::vector<mixer_port_config> config;
  j.get_to(config);
  add_ports_from_config(mixer, config);
}

} // namespace jack_minimixer::config

namespace jack_minimixer::gui {

inline wxString centibel_format(int centibel) {
  return wxString::Format("%+d.%ddb", centibel / 10, std::abs(centibel) % 10);
}

class volume_sliders_widget : public wxPanel {
  port_vector_base &port_vector;

public:
  explicit volume_sliders_widget(port_vector_base &port_vector,
                                 wxWindow *parent, wxWindowID id)
      : wxPanel(parent, id), port_vector(port_vector) {

    wxGridBagSizer *sizer = new wxGridBagSizer(10, 10);
    // Possibly add locking once dynamic port setup is here
    unsigned int column = 0;
    for (const auto &vport : port_vector.ports) {
      //      sizer->AddGrowableCol(column, 1);
      const jack_port_base &port = std::visit(
          [](const auto &val) -> const jack_port_base & { return val; }, vport);
      wxStaticText *text = new wxStaticText(this, wxID_ANY, port.name);
      int centibel_gain = multiplier_to_db_gain(port.amplification) * 10;
      wxStaticText *slider_value =
          new wxStaticText(this, wxID_ANY, centibel_format(centibel_gain));
      // The slider is a number of centibel (tenth of decibel)
      wxSlider *slider = new wxSlider(this, wxID_ANY, centibel_gain, -800, 200,
                                      wxDefaultPosition, wxDefaultSize,
                                      wxSL_VERTICAL | wxSL_INVERSE);
      slider->SetTick(0);
      sizer->Add(slider_value, wxGBPosition(0, column), wxDefaultSpan,
                 wxALIGN_CENTER);
      sizer->Add(slider, wxGBPosition(1, column), wxDefaultSpan, wxEXPAND);
      sizer->Add(text, wxGBPosition(2, column), wxDefaultSpan, wxALIGN_CENTER);
      sizer->AddGrowableCol(column);
      slider->Bind(wxEVT_SCROLL_CHANGED, [slider_value, column,
                                          &port_vector](wxScrollEvent &ev) {
        slider_value->SetLabel(centibel_format(ev.GetPosition()));
        auto lock = std::unique_lock(port_vector.mutex);
        std::visit([](auto &val) -> jack_port_base & { return val; },
                   port_vector.ports[column])
            .amplification =
            db_gain_to_multiplier((float)ev.GetPosition() / 10.0f);
      });
      column++;
    }
    sizer->AddGrowableRow(1);

    SetSizerAndFit(sizer);
  }
};

class mixer_main_window : public wxFrame {
  jack_mixer &mixer;

public:
  explicit mixer_main_window(jack_mixer &mixer, wxWindow *parent)
      : wxFrame(parent, -1, "jack_minimixer"), mixer(mixer) {
    wxBoxSizer *sizer = new wxBoxSizer(wxHORIZONTAL);
    wxStaticBoxSizer *inputBoxSizer =
        new wxStaticBoxSizer(wxHORIZONTAL, this, "Input");
    wxStaticBoxSizer *outputBoxSizer =
        new wxStaticBoxSizer(wxHORIZONTAL, this, "Output");
    inputBoxSizer->Add(
        new jack_minimixer::gui::volume_sliders_widget(
            mixer.get_port_vector<input_port_tag>(), this, wxID_ANY),
        1, wxEXPAND);
    outputBoxSizer->Add(
        new jack_minimixer::gui::volume_sliders_widget(
            mixer.get_port_vector<output_port_tag>(), this, wxID_ANY),
        1, wxEXPAND);
    sizer->Add(inputBoxSizer, 1, wxEXPAND);
    sizer->AddSpacer(2);
    sizer->Add(outputBoxSizer, 1, wxEXPAND);
    SetSizerAndFit(sizer);
  }
};

} // namespace jack_minimixer::gui

using namespace jack_minimixer;

static const wxCmdLineEntryDesc cmdline_desc[] = {
    {wxCMD_LINE_OPTION, "c", "config", "path to the config file",
     wxCMD_LINE_VAL_STRING, 0},
    {wxCMD_LINE_NONE}};

class mixer_app : public wxApp {
  jack_mixer mixer;
  wxFrame *frame;
  std::filesystem::path config_file_path;

public:
  bool OnInit() override {
    wxStandardPaths::Get().SetFileLayout(wxStandardPathsBase::FileLayout_XDG);
    if (!wxApp::OnInit()) {
      return false;
    }
    mixer.shutdown_callback = [this](jack_status_t code, const char *reason) {
      wxLogInfo(wxString::Format("Jack disconnected, code %d, reason: %s\n",
                                 code, reason));
      this->ExitMainLoop();
    };
    config::add_ports_from_config(mixer, config_file_path);
    mixer.activate();

    frame = new jack_minimixer::gui::mixer_main_window(mixer, nullptr);
    frame->Show();

    return true;
  }
  void OnInitCmdLine(wxCmdLineParser &parser) override {
    parser.SetDesc(cmdline_desc);
    parser.SetSwitchChars(wxT("-"));
  }
  bool OnCmdLineParsed(wxCmdLineParser &parser) override {
    if (wxString path; parser.Found("c", &path)) {
      config_file_path = path.utf8_string();
    } else {
      config_file_path =
          std::filesystem::path(
              wxStandardPaths::Get().GetUserConfigDir().utf8_string()) /
          "jack_minimixer" / "ports.json";
    }
    return true;
  }

  void OnUnhandledException() override { throw; }
};

wxIMPLEMENT_APP(mixer_app);
